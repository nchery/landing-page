# Landing-page

## Pre-requisite

node/npm

## Start locally the project

```
git clone https://gitlab.com/nchery/landing-page.git
npm i
npm run dev
Open http://localhost:3000
```

## Checkout the deployed Version

https://landing-page-git-master.cherynicolas.now.sh/
