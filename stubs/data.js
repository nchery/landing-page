export const data = {
    menu: [
      { name: "link 1", url: ""},
      { name: "link 2", url: ""},
      { name: "link 3", url: ""},
      { name: "link 4", url: ""},
      { name: "link 5", url: ""},
      { name: "link 6", url: ""},
    ],
    companyDescription: {
        title: "Who we are",
        description: "We are one of the best bicycle companies in the world. We provide the best quality products of bicycle and no one can be compared by our service because we are the best and we will be. We devliver all the products with branch new quality and services. Any product is available in diffrent colors and they are very good looking. We tale very less delivery charge in comparison with other companies.",
        imageUrl: ""
    },
    featuredProducts: {
        title: "Featured Products",
        products: [
            {type: "Mountain Bike"},
            {type: "Frames"},
            {type: "aero system"},
            {type: "aero system"},
        ]
    },
    shopByParts: {
      title: "Shop by parts",
      catalog:[
          { selection: "Race Road/TT bikes" },
          { selection: "Road Mountain bikes" },
          { selection: "Lifestyle Bikes"},
          { selection: "Bespoke Bikes"}
      ]
    },
    maintenance: {},
    articles: {
        title: "Lastest Articles",
        articles: [
            { type: "lifestyle", comments:'5', date:'1 sept 2017', title: "blabal", body: "lorem ipsum ................."},
            { type: "how to", comments: '5', date: '5 sept 2017', title: 'Zou', body: 'lloooorem ipsum etc...............'}
        ]
    },
    philosophy: {title:"", quote:""}
};
