// Components
import Layout from '../components/Layout/Layout';
import Section from '../components/Section/Section';
import Banner from '../components/Banner/Banner';
import Company from '../components/Company/Company';
import FeaturedProducts from '../components/FeaturedProducts/FeaturedProducts';

// Global Style
import GlobalStyle from '../components/Global';

// stub
import { data } from '../stubs/data';

function LandingPage(props) {
    const { companyDescription, featuredProducts, shopByParts ,maintenance, articles, menu } = props;
    return (
    <Layout menu={menu}>
        <Banner />
        <Company {...companyDescription} />
        <Section title={featuredProducts.title} >
          <FeaturedProducts />
        </Section>
        <Section title={shopByParts.title} />
        <Section title={articles.title} />
        <GlobalStyle />
    </Layout>
    )
}

// mocking fetch of data to initialize the app
LandingPage.getInitialProps = async function() {
    return data;
};

export default LandingPage;
