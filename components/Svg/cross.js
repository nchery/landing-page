
function Cross({width, height}) {
  return (
    <svg fill='white' width={width} height={height} version="1.1" id="Capa_1" x="0px" y="0px"
    	 viewBox="0 0 31.112 31.112">
    <polygon points="31.112,1.414 29.698,0 15.556,14.142 1.414,0 0,1.414 14.142,15.556 0,29.698 1.414,31.112 15.556,16.97
    	29.698,31.112 31.112,29.698 16.97,15.556 "/>
    </svg>
  )
}

export default Cross;
