import Link from 'next/link';
import Carousel from 'nuka-carousel';
import BackArrow from '../Svg/backArrow';
import ForwardArrow from '../Svg/forwardArrow';
import Button from '../Button/Button';
import s from './banner.less';

function Banner() {
  return (
    <Carousel
      renderTopRightControls={({ currentSlide }) => (
        <div className={`${s.bannerTitle}`}>
          <p>AWESOME BICYCLE PRODUCTS</p>
          <Link href="/account">
            <Button />
          </Link>
        </div>
      )}
      renderCenterLeftControls={({ previousSlide }) => (
        <div className={`${s.circleArrow} ${s.positionLeft}`} onClick={previousSlide}>
            <BackArrow width="15" height="15" style={{ transform: 'translate(65%)'}}>Previous</BackArrow>
        </div>
      )}
      renderCenterRightControls={({ nextSlide }) => (
        <div className={`${s.circleArrow} ${s.positionRight}`} onClick={nextSlide}>
            <ForwardArrow width="15" height="15" style={{ transform: 'translate(85%)'}}>Next</ForwardArrow>
        </div>
      )}
    >
      <img src="https://picsum.photos/id/1023/3955/2094" />
      <img src="https://picsum.photos/id/203/3955/2094" />
      <img src="https://picsum.photos/id/839/3955/2094" />
    </Carousel>
  )
}

export default Banner;
