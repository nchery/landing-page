import s from './menu.less'
import Link from 'next/link';

function Menu(props) {
  const list = props.menu.map((listItem, id) => (
    <li key={`Menu__${id}`}>
      <Link href={listItem.url}>
        <a title={listItem.name}>{listItem.name}</a>
      </Link>
    </li>
  ))
  return (
    <nav className={`${s.menu} ${props.menuOpened ? s.animation : ''}`}>
      <div className="main-menu">
        <ul>
          {list}
        </ul>
      </div>
    </nav>
  )
}

export default Menu;
