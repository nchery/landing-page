import s from './burger.less';

function Burger(props) {
  const { menuOpened } = props;
  return (
    <a className={`${s.burger}`} href="#" title="Toggle Menu">
      <span className={`${menuOpened ? s.crossBurger : ''}`}></span>
    </a>
  )
}

export default Burger;
