import s from './section.less';

function Section(props) {
    const title = props.title.toUpperCase();
    return (
        <section className={`${s.section} row`}>
            <h3>{title}</h3>
            {props.children}
        </section>
    )
}

export default Section;
