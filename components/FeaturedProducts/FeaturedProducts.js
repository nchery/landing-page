import s from './featuredproducts.less';

function FeaturedProducts(props) {
  return (
    <div style={{width: "100%"}} className='row'>
      <div className={`col-xs-12 col-sm-9`}>
        <div style={{height: '250px', backgroundColor: "rgba(125,130,134,1)", width: '100%'}}>
          <div>
            <div>Type</div>
            <div>Part name</div>
            <div>
              <div>Buy Button</div>
              <div>Add to wishlist</div>
              <div>Price</div>
            </div>
          </div>
          <div></div>
        </div>
        <div className="row" style={{height: '250px', width: '100%'}}>
          <div className="col-xs-6" style={{height: '250px', backgroundColor: 'rgba(60,29,35,1)', width: '100%'}}>
          </div>
          <div className="col-xs-6" style={{height: '250px', width: '100%', backgroundColor: 'rgba(251,188,91,1)'}}>
          </div>
        </div>
      </div>
      <div className={`col-xs-12 col-sm-3`} style={{height: "500px", backgroundColor: "rgba(51,54,69,1)"}}>
      </div>
    </div>
  )
}

export default FeaturedProducts;
