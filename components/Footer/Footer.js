import Link from 'next/link'

const linkStyle = {
  marginRight: 15
}

function Footer() {
  return (
    <footer>
      <div>
        Logo
        <Link href="/wishlist">
          <a style={linkStyle}>Guides</a>
        </Link>
        <Link href="/wishlist">
          <a style={linkStyle}>Terms of Use</a>
        </Link>
        <Link href="/wishlist">
          <a style={linkStyle}>Privacy Policy</a>
        </Link>
      </div>
      <div>
        Quick Link
        <Link href="/search">
          <a style={linkStyle}>Search</a>
        </Link>
        <Link href="/shipping">
          <a style={linkStyle}>Shipping</a>
        </Link>
      </div>
    </footer>
  )
}

export default Footer
