import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import Menu from '../Menu/Menu';
import s from './layout.less';

class Layout extends React.Component {
  constructor(props){
    super();
    this.state = { menuOpened: false }
    this.displayMenu = this.displayMenu.bind(this);
  }

  displayMenu(){
    this.setState({ menuOpened: !this.state.menuOpened })
  }

  render(){
    const { menuOpened } = this.state;
    return (
      <div className={`${menuOpened ? s.displayMenu: ''}`}>
        <Menu menuOpened={menuOpened} menu={this.props.menu} />
        <main className={`${s.animation} grid_container`}>
          <Header displayMenu={this.displayMenu} menuOpened={menuOpened} />
          {this.props.children}
          <Footer />
        </main>
      </div>
    )
  }
}

export default Layout;
