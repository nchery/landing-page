import Button from '../Button/Button';
import s from './company.less';

function Company(props) {
  return (
    <section className={`${s.whoweare} row`}>
      <article className='col-xs-12 col-sm-6 col-md-6 col-lg-6'>
        <h2>{props.title.toUpperCase()}</h2>
        <p>{props.description}</p>
        <Button />
      </article>
      <aside className="col-sm-6 col-md-6 col-lg-6">
        <img src="https://picsum.photos/id/1077/300" />
      </aside>
    </section>
  )
}

export default Company;
