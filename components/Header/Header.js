import Link from 'next/link';
import Glass from '../Svg/glass';
import Burger from '../Burger/Burger';
import Account from '../Svg/account';
import Heart from '../Svg/heart';
import Cart from '../Svg/cart';
import Cross from '../Svg/cross';

// styles
import s from "./header.less";

class Header extends React.Component {
  constructor(props){
    super()
    this.state = { search: false }
    this.displaySearch = this.displaySearch.bind(this);
    this.hideSearch = this.hideSearch.bind(this);
  }

  displaySearch() {
    if (!this.props.menuOpened) {
      this.setState({ search: !this.state.search });
    }
  }

  hideSearch() {
    this.setState({ search : !this.state.search });
  }

  render() {
    return (
      <header className={`${s.header}`}>
        {this.state.search &&
          <div className={`row`}>
            <div className={`col-xs-12 col-sm-12 col-md-12 col-lg-12`}>
              <div className={`${s.layoutInput}`}>
                <div className={`${s.spacing} ${s.positionIcon} ${s.pointer}`} onClick={this.hideSearch}>
                  <Cross width="21" height="21" />
                </div>
                <input className={`${s.searchField}`} autoFocus type="search" placeholder="Type you search" />
              </div>
            </div>
          </div>
        }

        {!this.state.search &&
          <div className={`row`}>
            <div className={`col-xs-4 col-sm-3 col-md-3 col-lg-3`}>
              <div className={`${s.layoutLeft}`}>
                <div className={`${s.separatorRight} ${s.spacing} ${s.positionIcon} ${s.pointer}`} onClick={this.displaySearch}>
                  <Glass width="21" height="21" />
                </div>
                <div className={`${s.layoutBurgerMenu} ${s.separatorRight}`} onClick={this.props.displayMenu}>
                  <div className={`${s.positionIcon}`}>
                    <Burger menuOpened={this.props.menuOpened} />
                  </div>
                  <div className={`${s.layoutText}`}>
                    <span className={`${s.menuText}`}>Shop Category</span>
                  </div>
                </div>
              </div>
            </div>
            <div className={`col-xs-4 col-sm-6`}>
              <div className={`${s.layoutLogo}`}>
                <h1>
                  <span>BICYCLE COMPANY</span>
                </h1>
              </div>
            </div>
            <div className={`col-xs-4 col-sm-3 col-md-3 col-lg-3`}>
              <div className={`${s.layoutRight}`}>
                <Link href="/account">
                  <a className={`${s.separatorLeft} ${s.separatorRight} ${s.spacing} ${s.positionIcon}`}>
                    <Account width="21" height="21" />
                  </a>
                </Link>
                <Link href="/wishlist">
                  <a className={`${s.separatorRight} ${s.spacing} ${s.iconToggle} ${s.positionIcon}`}>
                    <Heart width="21" height="21" />
                  </a>
                </Link>
                <Link href="/cart">
                  <a className={`${s.spacing} ${s.positionIcon}`}>
                    <Cart width="21" height="21" />
                  </a>
                </Link>
              </div>
            </div>
          </div>
        }
      </header>
    )
  }
}

export default Header;
